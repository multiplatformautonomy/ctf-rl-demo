
# Implementation from https://github.com/vmayoral/basic_reinforcement_learning
# Modified to allow for state-specific actions

import random
import math
import copy
import sys

class QLearn(object):
    """
        Traditional QLearning with Single-Policy Learning based on a
        linear combination of reward components.
    """
    def __init__(self, epsilon, alpha, gamma, get_actions_function,
            state_function=lambda results:tuple(results[0]), reward_function = lambda results: results[1],
            done_function=lambda results: results[2], epsilon_step = 1000, epsilon_end = 0.0,
            use_epsilon_decay = False):
        """ 
        Parameters:
        ___________

        epsilon : float
            A value between 0 and 1 that represents the chance of choosing a random action
        alpha : float
            A value between 0 and 1 that represents the learning rate in Qlearning update step.
        gamma : float
            A value between 0 and 1 that represents the discount factor in Qlearning update step. 
        get_actions_function: function
            A function that takes a single state tuple as a parameter and returns the possible actions
        state_function : function
            A function that is passed the results of environment.step() and extracts the state as tuple
        reward_function : function
            A function that is passed the results of environment.step() and extracts the reward
        done_function : function
            A function that is passed the results of environment.step() and extracts the done boolean
        epsilon_step  : int
            A value that is used to calculate the rate that epsilon decays. Smaller values result in faster decay. Will be
            ignored if use_epsilon_decay is False
        epsilon_end : float
            The smallest value that epsilon will reach in epsilon decay. Will be ignored if use_epsilon_decay is false
        use_epsilon_decay: boolean
            Determines whether epsilon should decay over time
        """

        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.get_actions_function = get_actions_function
        self.state_function = state_function
        self.reward_function = reward_function
        self.done_function = done_function
        self.epsilon_step = epsilon_step
        self.epsilon_end = epsilon_end
        self.use_epsilon_decay = use_epsilon_decay
        self.epsilon_current = epsilon
      
        self.q = {} # map of (state, action) pairs to q values
        self.t = 0 # time elapsed since model started training
        self.delta = 0
        self.learning_mode = True

    def getQ(self, state, action):
        """ Returns Q value for state-action pair. """
        return self.q.get((state, action), 0.0)

    def learnQ(self, state, action, reward, value):
        """
        Q-learning:
            Q(s, a) += alpha * (reward(s,a) + max(Q(s') - Q(s,a))
        """
        oldv = self.q.get((state,action), None)
        if oldv is None:
            self.q[(state, action)] = reward
            self.delta = max(self.delta, abs(reward))
        else:
            self.q[(state, action)] = oldv + self.alpha * (value - oldv)
            self.delta = max(self.delta, abs(oldv-self.q[(state, action)]))

    def choose_action_egreedy(self, state, return_q=False):
        """ Chooses an action using the epsilon-greedy strategy. """
        eps_threshold = self.epsilon
        if self.use_epsilon_decay:
            eps_threshold = self.epsilon_end + (self.epsilon - self.epsilon_end) *\
            math.exp(-1. * self.t / self.epsilon_step)
            self.epsilon_current = eps_threshold
 
        if random.random() < eps_threshold:
            # altered to remove magic numbers and change back to a truly random choice
            if return_q:
                act = random.choice(self.get_actions_function(state))
                qval = self.getQ(state, act)
                return act, qval
            return random.choice(self.get_actions_function(state))
        return None

    def choose_action(self, state, return_q=False, rand_method=choose_action_egreedy):
        """ Chooses an action using the random exploration strategy or the best possible
            if the exploration strategy fails to return an exploratory action or rand_method == None. 
        """
        qvals = [self.getQ(state, a) for a in self.get_actions_function(state)]
        max_q = max(qvals)
        action = None

        if rand_method is not None:
            action = rand_method(self, state, return_q)

        if action is None:
            count = qvals.count(max_q)
            # In case there're several state-action max values
            # we select a random one among them
            if count > 1:
                best = [i for i in range(len(self.get_actions_function(state))) if qvals[i] == max_q]
                i = random.choice(best)
            else:
                i = qvals.index(max_q)

            action = self.get_actions_function(state)[i]

        if return_q: 
            return action, qvals
        return action

    def learn(self, state1, action1, reward, state2):
        """ Updates Q values based on maximizing the future Q values """
        maxqnew = max([self.getQ(state2, a) for a in self.get_actions_function(state2)])
        self.learnQ(state1, action1, reward, reward + self.gamma*maxqnew)

    def train(self, start_state, environment, max_iter=None):
        """ Runs one training episode """
        use_max_iter = max_iter is not None
        iterations = 0
        new_state = start_state
        done = False
        self.delta = 0
        score = 0
        while not done and (not use_max_iter or iterations < max_iter):
            new_state, reward, done, time= self.train_step(new_state, environment, rand_method=QLearn.choose_action_egreedy, learning_mode=True)
            iterations = iterations + 1
            self.t = self.t + 1
            score = score + reward
        return (new_state, reward, self.t, score, time)

    def run (self, start_state, environment, max_iter=None):
        """ Runs the current model in non learning mode and chooses actions that return maximum q values"""
        use_max_iter = max_iter is not None
        iterations = 0
        new_state = start_state
        done = False
        score = 0

        while not done and (not use_max_iter or iterations < max_iter):
            new_state, reward, done, time = self.train_step(new_state, environment, rand_method=None, learning_mode=False)
            iterations = iterations + 1
            score = score + reward
        return (new_state, reward, self.t, score, time)
    
    def train_step(self, state, environment, rand_method=choose_action_egreedy, learning_mode=True):
        """ Performs one step. Learns from the results if learning_mode is True"""
        # Choose the action
        action = self.choose_action(state, False, rand_method)
        state1 = copy.copy(state)
        state2, reward, done, info  = environment.step(action)

        state2 = tuple(state2)
        # Call learn
        if learning_mode:
            self.learn(state1, action, reward, state2)

        return state2, reward, done, info

import sys
import copy
import gym
import scrimmage.utils
from qlearn import QLearn
import time
import math
import os.path

class CTF_Model:
    model = None
    action_space = 0
    
    def load_model():
        if os.path.exists('model.pickle'):
            import pickle
            with open('model.pickle', 'rb') as handle:
                CTF_Model.model = pickle.load(handle)
        else:
            print("Model Does NOT exist") 
    
    def save_model(model):
        import pickle
        with open('model.pickle', 'wb') as handle:
            pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

def action_func(obs):
    action = 0
    if CTF_Model.model is not None:
        obs_tuple = tuple(obs)
        qvals = [CTF_Model.model.get((obs_tuple, a), 0.0) for a in list(range(CTF_Model.action_space))]
        action = qvals.index(max(qvals))
    else:
        print("ERROR: Model Does Not Exist")
    return action

def return_action_func(action_space, obs_space, params=None):
    CTF_Model.load_model()
    count_nan = list(CTF_Model.model.values()).count(math.nan)
    CTF_Model.action_space = action_space.n
    print("ctf_pytorch:return_action_func - Model Loaded - count_nan=", count_nan, ", len=", len(CTF_Model.model), ",action_space=", action_space)
    return action_func

def get_actions(state):
    return list(range(8))

def test_openai(train_mode=True):
    try:
        env = gym.make('scrimmage-ctf-v0')
    except gym.error.Error:
        mission_file = scrimmage.utils.find_mission('ctf-qlearn.xml')

        gym.envs.register(
            id='scrimmage-ctf-v0',
            entry_point='scrimmage.bindings:ScrimmageOpenAIEnv',
            max_episode_steps=1e9,
            reward_threshold=1e9,
            kwargs={"mission_file": mission_file}
        )
        env = gym.make('scrimmage-ctf-v0')

    ALPHA = 0.9 
    GAMMA = 0.8 
    EPSILON = .9 
    EPSILON_STEP = 400000 
    EPSILON_END = 0.0

    qlearn = QLearn(EPSILON, ALPHA, GAMMA, get_actions_function=get_actions, use_epsilon_decay=True, epsilon_step=EPSILON_STEP, epsilon_end=EPSILON_END)
    
    num_episodes = 3000
    episode = 0
    prev_score = 0
    tol = 0.005
    converge_end = 4
    converge_start = 0
    delta = 0
    max_iter = None

    if train_mode:
        print("----LEARNING MODE----")
        while converge_start < converge_end:
            endState, reward, time, score, info = qlearn.train(tuple(env.reset()), env, max_iter=max_iter)

            if (math.isclose(score, prev_score, abs_tol=tol)):
                converge_start += 1
            else:
                converge_start = 0
            prev_score = score
            delta = qlearn.delta
            episode = episode + 1
            if episode > num_episodes:
                break
        CTF_Model.save_model(qlearn.q)
    else:
        print("-- NON LEARNING MODE----")
        CTF_Model.load_model()
        qlearn.q = CTF_Model.model
        endState, reward, time, score, t = qlearn.run(tuple(env.reset()), env, max_iter=None) 
    
    env.close()

if __name__ == '__main__':
    start_time = time.time()
    test_openai(train_mode=False)
    print("total time: ", time.time() - start_time)
